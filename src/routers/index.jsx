import { Suspense, lazy } from 'react';
import { createBrowserRouter } from 'react-router-dom';

import Loading from '@/components/loading';
import ConfigLayout from '@/components/layout/ConfigLayout';
import AppLayout from '@/components/layout/AppLayout';

const AppPage = lazy(() => import('@/page/App'));
const ErrorPage = lazy(() => import('@/error'));

const router = createBrowserRouter([
  {
    path: '/',
    element: <ConfigLayout />,
    errorElement: <ErrorPage />,
    children: [
      {
        element: <AppLayout />,
        children: [
          {
            index: true,
            element: (
              <Suspense fallback={<Loading open={true} />}>
                <AppPage />
              </Suspense>
            ),
          },
        ],
      },
    ],
  },
]);

export default router;
