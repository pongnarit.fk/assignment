import { Container, Typography } from '@mui/material';
import { useRouteError } from 'react-router-dom';

const containerStyle = {
  height: '100vh',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: ' center',
};

export default function ErrorPage() {
  const error = useRouteError();

  return (
    <Container sx={containerStyle}>
      <Typography
        align="center"
        variant="h1"
      >
        Oops!
      </Typography>
      <Typography
        align="center"
        variant="p"
      >
        Sorry, an unexpected error has occurred.
      </Typography>
      <Typography
        align="center"
        variant="p"
      >
        <i>{error.statusText || error.message}</i>
      </Typography>
    </Container>
  );
}
