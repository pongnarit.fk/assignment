import PropTypes from 'prop-types';
import { useMemo, useState } from 'react';

import { OrderContext } from '@/hook/useOrder';

const defualtFilter = {
  page: 1,
  pageSize: 20,
  name: '',
  set: '',
  rarity: '',
  type: '',
};

export default function OrderProvider({ children }) {
  const [isDrawerOpened, setDrawMenuOpen] = useState(false);
  const [order, setOrder] = useState([]);
  const [filter, setFilter] = useState(defualtFilter);

  const addOrder = (data) => {
    const index = order.map((e) => e.id).indexOf(data.id);
    if (order.length === 0 || index < 0) setOrder([...order, { ...data, amount: 1 }]);
    if (index > -1) {
      const updateOrder = [...order];
      updateOrder[index].amount = updateOrder[index].amount + 1;
      setOrder([...updateOrder]);
    }
  };

  const removeOrder = (id) => {
    const index = order.map((e) => e.id).indexOf(id);
    if (order[index].amount > 1) {
      const updateOrder = [...order];
      updateOrder[index].amount = updateOrder[index].amount - 1;
      setOrder([...updateOrder]);
    } else {
      const array = order.filter((item) => item.id !== id);
      setOrder([...array]);
    }
  };

  const total = useMemo(() => {
    let cardTotal = order.reduce((prev, curr) => prev + curr.amount, 0);
    let totalArray = order.map((item) => item.cardmarket.prices.averageSellPrice * item.amount);
    let priceTotal = totalArray.reduce((acc, num) => acc + num, 0);
    return { cardTotal: cardTotal, priceTotal: priceTotal };
  }, [order]);

  const clearOrder = () => {
    setOrder([]);
  };

  const value = {
    isDrawerOpened,
    setDrawMenuOpen,
    order,
    total,
    addOrder,
    removeOrder,
    clearOrder,
    filter,
    setFilter,
    defualtFilter,
  };

  return <OrderContext.Provider value={value}>{children}</OrderContext.Provider>;
}

OrderProvider.propTypes = {
  children: PropTypes.node,
};
