import { useQuery } from 'react-query';
import { Box, Grid, Pagination } from '@mui/material';

import '@/App.css';
import { useOrder } from '@/hook/useOrder';
import { fetchPokemon } from '@/api/pokemon';

import SubHeader from '@/components/header/subHeader';
import PokemonCard from '@/components/card/pokemonCard';
import PokemonSkeleton from '@/components/skeleton/PokemonSkeleton';
import useCustomSnackbar from '@/components/snackbar/useCustomSnackbar';

const skeletonItems = [...Array(20).fill('')].map((item, index) => <PokemonSkeleton key={index} />);

function App() {
  const snackbar = useCustomSnackbar();
  const { filter, setFilter } = useOrder();

  const { data: pokemonData = {} } = useQuery({
    queryKey: ['pokemon', filter],
    queryFn: () => fetchPokemon(filter),
    onError: (e) => {
      snackbar.showError(e.message);
    },
  });

  const onChangePage = (page) => {
    setFilter({ ...filter, page: page });
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  };

  const gameListElement = pokemonData?.data?.map((item, index) => (
    <Grid
      key={item.name + index}
      item
      xs={12}
      sm={4}
      md={2}
    >
      <PokemonCard
        key={item.id + index}
        data={item}
      />
    </Grid>
  ));

  return (
    <>
      <SubHeader />
      <Grid
        container
        spacing={2}
        height="100%"
        sx={{ pt: 1 }}
      >
        {gameListElement || skeletonItems}
      </Grid>
      {pokemonData.totalCount > 0 && (
        <Box sx={{ display: 'flex', justifyContent: 'center', my: 2 }}>
          <Pagination
            count={pokemonData.totalCount ? Math.round(pokemonData.totalCount / 20) : 1}
            variant="outlined"
            shape="rounded"
            page={pokemonData.page || 1}
            onChange={(_, page) => onChangePage(page)}
            size="small"
          />
        </Box>
      )}
    </>
  );
}

export default App;
