import instance from '@/config/instance';

export const fetchSet = async () => {
  const { data } = await instance.get('/v2/sets');
  return data;
};

export const fetchRarity = async () => {
  const { data } = await instance.get('/v2/rarities');
  return data;
};

export const fetchType = async () => {
  const { data } = await instance.get('/v2/types');
  return data;
};
