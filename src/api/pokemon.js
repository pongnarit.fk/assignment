import instance from "@/config/instance";
import { replaceSpaces } from "@/helper/format";

export const fetchPokemon = async (params) => {
  const { data } = await instance.get("/v2/cards", {
    params: {
      page: params.page,
      pageSize: params.pageSize,
      q: [
        params.name && `name:${replaceSpaces(params.name)}`,
        params.set && `set.id:${replaceSpaces(params.set)}`,
        params.type && `types:${replaceSpaces(params.type)}`,
        params.rarity && `rarity:${replaceSpaces(params.rarity)}`,
      ]
        .filter(Boolean)
        .join(" "),
    },
  });
  return data;
};
