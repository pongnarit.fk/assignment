import PropTypes from 'prop-types';
import { Backdrop, CircularProgress } from '@mui/material';

const Loading = ({ open }) => {
  return (
    <Backdrop open={open}>
      <CircularProgress />
    </Backdrop>
  );
};

Loading.propTypes = {
  open: PropTypes.bool,
};

export default Loading;
