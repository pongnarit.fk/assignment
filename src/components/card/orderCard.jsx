import { Box, Button, Grid, Typography, useTheme } from '@mui/material';
import { LazyLoadImage } from 'react-lazy-load-image-component';

import { useOrder } from '@/hook/useOrder';
import { fCurrency } from '@/helper/format';

export default function OrderCard() {
  const theme = useTheme();
  const { order, addOrder, removeOrder } = useOrder();

  return (
    <>
      {!order.length && (
        <Typography
          textAlign="center"
          color={theme.palette.text.secondary}
          sx={{ my: 2 }}
        >
          No Order
        </Typography>
      )}
      {order.map((item, index) => (
        <Box
          key={index}
          sx={{ my: 2 }}
        >
          <Grid
            container
            spacing={1}
            columns={12}
            sx={{ my: 1 }}
          >
            <Grid
              item
              xs={2.7}
            >
              <Box sx={{ width: '44px', height: '66px' }}>
                <LazyLoadImage
                  src={item.images.small}
                  alt={item.name}
                  width={'100%'}
                  height={'auto'}
                  effect="blur"
                />
              </Box>
            </Grid>
            <Grid
              item
              xs={6.5}
            >
              <Typography noWrap>{item.name}</Typography>
              <Typography
                fontSize={12}
                color={theme.palette.text.secondary}
              >
                $ {item?.cardmarket?.prices?.averageSellPrice || 0.0}
              </Typography>
            </Grid>
            <Grid
              item
              xs={2.7}
              sx={{ display: 'flex', justifyContent: 'end' }}
            >
              $ {fCurrency(item?.cardmarket?.prices?.averageSellPrice * item.amount)}
            </Grid>
          </Grid>
          <Grid
            container
            spacing={1}
            columns={12}
          >
            <Grid
              item
              xs={2.7}
            >
              <Button
                variant="contained"
                fullWidth
                onClick={() => removeOrder(item.id)}
              >
                -
              </Button>
            </Grid>
            <Grid
              item
              xs={6.5}
            >
              <Button
                variant="contained"
                fullWidth
              >
                {item.amount}
              </Button>
            </Grid>
            <Grid
              item
              xs={2.7}
            >
              <Button
                variant="contained"
                fullWidth
                onClick={() => addOrder(item)}
              >
                +
              </Button>
            </Grid>
          </Grid>
        </Box>
      ))}
    </>
  );
}
