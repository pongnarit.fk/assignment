import PropTypes from 'prop-types';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import { Box, Button, Card, Typography, useTheme } from '@mui/material';

import { useOrder } from '@/hook/useOrder';

import bag from '@/assets/shopping-bag.svg';

export default function PokemonCard(props) {
  const theme = useTheme();
  const { data } = props;
  const { addOrder } = useOrder();

  const boxBackgroundStyle = {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
    height: '60%',
    borderRadius: '16px',
    backgroundImage: `linear-gradient(to top, ${theme.palette.background.card} 100%, transparent 0%)`,
    zIndex: -1,
  };

  return (
    <Card sx={{ boxShadow: 'none', borderRadius: '16px' }}>
      <Box
        sx={{
          pb: 2,
          px: 2,
          width: '100%',
          height: '100%',
          position: 'relative',
          overflow: 'hidden',
          zIndex: 1,
        }}
      >
        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
          <Box sx={{ width: { xs: '194.73px', sm: '102px' }, height: { xs: '270.81px', sm: '142px' } }}>
            <LazyLoadImage
              src={data.images.large}
              alt={data.name}
              width={'100%'}
              height={'auto'}
              effect="blur"
            />
          </Box>
        </Box>
        <Box sx={{ py: 2 }}>
          <Box sx={{ lineClamp: 2 }}>
            <Typography
              textAlign="center"
              fontSize={14}
              fontWeight={500}
              height={45}
              sx={{
                overflow: 'hidden',
                display: '-webkit-box',
                WebkitBoxOrient: 'vertical',
                WebkitLineClamp: 2,
              }}
            >
              {data.name || ''}
            </Typography>
          </Box>
          <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', color: theme.palette.text.secondary }}>
            <Typography fontSize={'0.8rem'}>$ {data?.cardmarket?.prices?.averageSellPrice || 0.0}</Typography>
            <Box sx={{ mx: 1 }}>&#x2022;</Box>
            <Typography
              fontSize={'0.8rem'}
              noWrap
            >
              - Cards
            </Typography>
          </Box>
        </Box>
        <Button
          variant="contained"
          sx={{ width: '100%', fontSize: '0.60rem' }}
          startIcon={
            <img
              src={bag}
              alt="bag"
            />
          }
          onClick={() => addOrder(data)}
        >
          Add to card
        </Button>
        <Box sx={boxBackgroundStyle} />
      </Box>
    </Card>
  );
}

PokemonCard.propTypes = {
  data: PropTypes.object,
};
