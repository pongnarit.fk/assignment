import { Grid, Skeleton } from '@mui/material';

function PokemonSkeleton() {
  return (
    <Grid
      item
      xs={12}
      sm={4}
      md={2}
    >
      <Skeleton
        sx={{
          borderRadius: 1,
          height: { xs: '336px', sm: '276px' },
          width: '100%',
        }}
        variant="rectangular"
      />
    </Grid>
  );
}

export default PokemonSkeleton;
