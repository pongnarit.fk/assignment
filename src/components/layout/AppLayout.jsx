import { Outlet } from 'react-router-dom';
import { Container, Divider, useTheme } from '@mui/material';

import Header from '@/components/header';
import AppDrawer from '@/components/header/drawer';

function AppLayout() {
  const theme = useTheme();

  return (
    <Container>
      <Header />
      <AppDrawer />
      <Divider sx={{ mt: 2, bgcolor: theme.palette.background.main }} />
      <Outlet />
    </Container>
  );
}

export default AppLayout;
