import { Suspense } from 'react';
import { Outlet } from 'react-router-dom';
import { CssBaseline } from '@mui/material';
import { Helmet, HelmetProvider } from 'react-helmet-async';
import { ThemeProvider } from '@mui/material/styles';
import { SnackbarProvider } from 'material-ui-snackbar-provider';

import appTheme from '@/config/theme';
import OrderProvider from '@/context/orderProvider';

import Loading from '@/components/loading/index';
import CustomSnackbar from '@/components/snackbar/CustomSnackbar';

export default function AppLayout() {
  return (
    <Suspense fallback={<Loading />}>
      <HelmetProvider>
        <Helmet>
          <title>Assignment</title>
        </Helmet>
        <ThemeProvider theme={appTheme}>
          <CssBaseline />
          <SnackbarProvider
            SnackbarComponent={CustomSnackbar}
            SnackbarProps={{
              anchorOrigin: { vertical: 'top', horizontal: 'right' },
              autoHideDuration: 3000,
            }}
          >
            <OrderProvider>
              <Outlet />
            </OrderProvider>
          </SnackbarProvider>
        </ThemeProvider>
      </HelmetProvider>
    </Suspense>
  );
}
