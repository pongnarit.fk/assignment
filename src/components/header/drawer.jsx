import CloseIcon from '@mui/icons-material/Close';
import { Box, Button, Divider, Drawer, Grid, Typography, useTheme } from '@mui/material';

import { useOrder } from '@/hook/useOrder';

import OrderCard from '@/components/card/orderCard';
import { fCurrency } from '@/helper/format';

export default function AppDrawer() {
  const theme = useTheme();
  const { total, clearOrder, isDrawerOpened, setDrawMenuOpen } = useOrder();
  return (
    <>
      <Drawer
        anchor="right"
        open={isDrawerOpened}
        onClose={() => setDrawMenuOpen(false)}
      >
        <Box sx={{ width: { xs: '100vw', sm: '50vw', md: '35vw' }, padding: 3 }}>
          <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
            <Box>
              <Typography
                fontSize={'26px'}
                fontWeight={600}
              >
                Card
              </Typography>
              <Typography
                fontSize={12}
                sx={{ textDecoration: 'underline', ':hover': { cursor: 'pointer' } }}
                onClick={() => clearOrder()}
              >
                Clear all
              </Typography>
            </Box>
            <Box>
              <Button
                variant="icon"
                onClick={() => setDrawMenuOpen(false)}
              >
                <CloseIcon />
              </Button>
            </Box>
          </Box>
        </Box>
        <Grid
          container
          spacing={2}
          sx={{ px: 3 }}
        >
          <Grid
            item
            xs={2.5}
          >
            Item
          </Grid>
          <Grid
            item
            xs={7}
          >
            Qty
          </Grid>
          <Grid
            item
            xs={2}
          >
            Price
          </Grid>
        </Grid>
        <Box sx={{ px: 3 }}>
          <Divider sx={{ mb: 1, px: 3 }} />
        </Box>
        <Box sx={{ px: 3, height: '50%', overflow: 'auto' }}>
          <OrderCard />
        </Box>
        <Box sx={{ px: 3 }}>
          <Box sx={{ display: 'flex', justifyContent: 'space-between', mt: 4 }}>
            <Typography color={theme.palette.text.secondary}>Total card amount</Typography>
            <Typography>{total.cardTotal || 0}</Typography>
          </Box>
          <Box sx={{ display: 'flex', justifyContent: 'space-between', mt: 2 }}>
            <Typography color={theme.palette.text.secondary}>Total price</Typography>
            <Typography>$ {fCurrency(total.priceTotal)}</Typography>
          </Box>
          <Button
            variant="icon"
            fullWidth
            sx={{ mt: 4 }}
          >
            Continue to Payment
          </Button>
        </Box>
      </Drawer>
    </>
  );
}
