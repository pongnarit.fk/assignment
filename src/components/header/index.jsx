import SearchIcon from "@mui/icons-material/Search";
import {
  Badge,
  Box,
  Button,
  FormControl,
  InputAdornment,
  OutlinedInput,
  Typography,
  useTheme,
} from "@mui/material";

import { useOrder } from "@/hook/useOrder";

import bag from "@/assets/shopping-bag.svg";

export default function Header() {
  const theme = useTheme();
  const { filter, setFilter, setDrawMenuOpen, total } = useOrder();

  return (
    <>
      <Box
        sx={{
          py: 2,
          justifyContent: "space-between",
          display: "flex",
          alignItems: "center",
        }}
      >
        <Typography sx={{ fontSize: "26px", fontWeight: 600 }}>
          Pokemon market
        </Typography>
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <FormControl
            fullWidth
            sx={{ display: { xs: "none", sm: "block" }, mr: 1 }}
          >
            <OutlinedInput
              value={filter.name}
              onChange={({ target }) =>
                setFilter({ ...filter, name: target.value })
              }
              startAdornment={
                <InputAdornment position="start">
                  <SearchIcon sx={{ color: theme.palette.common.white }} />
                </InputAdornment>
              }
              placeholder="Search By Name"
            />
          </FormControl>
          <Badge badgeContent={total.cardTotal} color="primary">
            <Button variant="icon" onClick={() => setDrawMenuOpen(true)}>
              <img style={{ width: 20, height: 20 }} src={bag} alt="bag" />
            </Button>
          </Badge>
        </Box>
      </Box>
      <FormControl fullWidth sx={{ display: { xs: "block", sm: "none" } }}>
        <OutlinedInput
          startAdornment={
            <InputAdornment position="start">
              <SearchIcon sx={{ color: theme.palette.common.white }} />
            </InputAdornment>
          }
          placeholder="Search By Name"
          fullWidth
        />
      </FormControl>
    </>
  );
}
