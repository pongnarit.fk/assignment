import { useQuery } from 'react-query';
import { Button, FormControl, Grid, InputLabel, MenuItem, Select, Typography } from '@mui/material';

import { useOrder } from '@/hook/useOrder';
import { fetchRarity, fetchSet, fetchType } from '@/api/type';

import useCustomSnackbar from '@/components/snackbar/useCustomSnackbar';

export default function SubHeader() {
  const snackbar = useCustomSnackbar();
  const { filter, setFilter, defualtFilter } = useOrder();

  const { data: setData = {} } = useQuery({
    queryKey: ['set'],
    queryFn: fetchSet,
    onError: (e) => {
      snackbar.showError(e.message);
    },
  });

  const { data: rarityData = {} } = useQuery({
    queryKey: ['rarity'],
    queryFn: fetchRarity,
    onError: (e) => {
      snackbar.showError(e.message);
    },
  });

  const { data: typeData = {} } = useQuery({
    queryKey: ['type'],
    queryFn: fetchType,
    onError: (e) => {
      snackbar.showError(e.message);
    },
  });

  return (
    <>
      <Grid
        container
        display="flex"
        alignItems="center"
        spacing={2}
        sx={{ my: 2 }}
      >
        <Grid
          item
          xs={12}
          md={6}
        >
          <Grid
            container
            display="flex"
            justifyContent="space-between"
            alignItems="center"
          >
            <Grid item>
              <Typography>Choose Card</Typography>
            </Grid>
            <Grid
              item
              display={{ xs: 'block', md: 'none' }}
            >
              <Button
                variant="icon"
                onClick={() => setFilter(defualtFilter)}
              >
                <Typography fontSize={12}>Clear</Typography>
              </Button>
            </Grid>
          </Grid>
        </Grid>
        <Grid
          item
          xs={12}
          md={6}
        >
          <Grid
            container
            spacing={2}
            justifyContent="flex-end"
            alignItems="center"
          >
            <Grid
              item
              xs={4}
              md={3}
            >
              <FormControl fullWidth>
                <InputLabel size="small">Set</InputLabel>
                <Select
                  size="small"
                  value={filter.set}
                  onChange={({ target }) => setFilter({ ...filter, set: target.value })}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  {setData &&
                    setData.data?.map((item, index) => (
                      <MenuItem
                        key={index}
                        value={item.id}
                      >
                        {item.name}
                      </MenuItem>
                    ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid
              item
              xs={4}
              md={3}
            >
              <FormControl fullWidth>
                <InputLabel size="small">Rarity</InputLabel>
                <Select
                  size="small"
                  value={filter.rarity}
                  onChange={({ target }) => setFilter({ ...filter, rarity: target.value })}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  {rarityData &&
                    rarityData.data?.map((item, index) => (
                      <MenuItem
                        key={index}
                        value={item}
                      >
                        {item}
                      </MenuItem>
                    ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid
              item
              xs={4}
              md={3}
            >
              <FormControl fullWidth>
                <InputLabel size="small">Type</InputLabel>
                <Select
                  size="small"
                  value={filter.type}
                  onChange={({ target }) => setFilter({ ...filter, type: target.value })}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  {typeData &&
                    typeData.data?.map((item, index) => (
                      <MenuItem
                        key={index}
                        value={item}
                      >
                        {item}
                      </MenuItem>
                    ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid
              item
              display={{ xs: 'none', md: 'block' }}
            >
              <Button
                variant="icon"
                onClick={() => setFilter(defualtFilter)}
              >
                <Typography fontSize={12}>Clear</Typography>
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
