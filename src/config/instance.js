import axios from 'axios';

const API_URL = 'https://api.pokemontcg.io';
const API_KEY = '7ec2803b-1628-4e47-bc7c-2cbd539042b8';

const instance = axios.create({
  baseURL: API_URL,
  headers: {
    'X-Api-Key': API_KEY,
    'Content-Type': 'application/json',
  },
});

export default instance;
