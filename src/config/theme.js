import { createTheme } from '@mui/material/styles';

const color = {
  text: {
    primary: '#FFFFFF',
    secondary: '#ABBBC2',
  },
  bg: {
    default: '#252836',
    secondary: '#EA7C69',
    tertiary: 'rgba(255, 255, 255, 0.08)',
    card: '#1F1D2B',
  },
};

const appTheme = createTheme({
  palette: {
    text: {
      primary: color.text.primary,
      secondary: color.text.secondary,
    },
    background: {
      default: color.bg.default,
      primary: color.bg.secondary,
      disable: color.bg.tertiary,
      card: color.bg.card,
    },
  },
  typography: {
    fontFamily: 'Poppins, sans-serif',
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          borderRadius: '8px',
        },
        icon: {
          minWidth: '48px',
          minHeight: '48px',
          backgroundColor: color.bg.secondary,
          boxShadow: `0px 8px 24px rgba(234, 124, 105, 0.32)`,
          ':hover': {
            backgroundColor: color.bg.secondary,
          },
        },
        contained: {
          backgroundColor: color.bg.tertiary,
        },
      },
    },
    MuiPaper: {
      styleOverrides: {
        root: {
          backgroundColor: color.bg.default,
        },
      },
    },
    MuiDivider: {
      styleOverrides: {
        root: {
          backgroundColor: color.bg.tertiary,
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          '&.Mui-focused .MuiOutlinedInput-notchedOutline': {
            color: color.text.primary,
            borderColor: color.text.primary,
          },
        },
      },
    },
    MuiFormLabel: {
      styleOverrides: {
        root: {
          '&.Mui-focused': {
            color: color.text.primary,
          },
        },
      },
    },
  },
});

export default appTheme;
